Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ETLegacy
Source: https://github.com/etlegacy/etlegacy
Files-Excluded:
 app
 gradle
 misc/wininstaller
Comment:
 The embedded libraries were replaced by Debian's system libraries. Windows and
 MacOS specific files, which are useless for Debian users, were removed.
 .
 All files in etmain are still covered by the original Wolfenstein: Enemy
 Territory EULA and thus they are non-distributable for Debian. Without them
 the game would be unusable hence there is no way this game can be packaged for
 Debian at the present time.

Files: *
Copyright: 1999-2010 id Software LLC, a ZeniMax Media company.
           2012 Stephen Larroque <lrq3000@gmail.com>
           2012 ET:L dev team <mail@etlegacy.com>
           2011 Dusan Jocic
           2010-2011 Robert Beckebans <trebor_7@users.sourceforge.net>
           2012-2024 ET:Legacy Team <mail@etlegacy.com>
License: GPL-3+

Files: docs/*
       misc/*
Copyright: 2012-2024 ET:Legacy Team <mail@etlegacy.com>
License: GPL-3

Files:
 src/tinygettext/*.cpp
 src/tinygettext/tinygettext/*.hpp
Copyright: 2006-2009 Ingo Ruhnke <grumbel@gmx.de>
License: Zlib

Files: src/game/g_mdx.h
       src/game/g_mdx_lut.h
Copyright: 2003-2005 Christopher Lais (aka "Zinx Verituse")
License: Lais-License
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
 3. Modified source for this software, as used in any binaries you have
    distributed, must be provided on request, free of charge and/or penalty.
 .
 4. This notice may not be removed or altered from any source distribution.

Files: cmake/FindLua.cmake
       cmake/FindSDL2.cmake
       cmake/FindTheora.cmake
Copyright: 2003-2009 Kitware, Inc.
           2012 2007-2009 Kitware, Inc. Modified to support Lua 5.2 by LuaDist
License: BSD-3-clause

Files: cmake/FindMiniZip.cmake
Copyright: 2012, John Schember <john@nachtimwald.com>
           2011, Dan Horák <dan[at]danny.cz>
           2012, Hans de Goede <hdegoede@redhat.com>
License: BSD-3-clause

Files: cmake/FindSQLite3.cmake
Copyright: 2007-2009 Peter Kapec <kapecp@gmail.com>
License: Expat

Files: src/game/g_mdx.c
Copyright: 2003-2005 Christopher Lais (aka "Zinx Verituse")
           modified by NQ & ET
License: Lais-License-modified
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
 3. Modified source for this software, as used in any binaries you have
    distributed, must be provided on request, free of charge and/or penalty.
 .
 4. This notice may not be removed or altered from any source distribution.
 .
 5. This software has been altered by NQ & ET: Legacy team and must not be
    misrepresented as being the original software.

Files: src/qcommon/md5.c
Copyright: no copyright is claimed
License: public-domain
 The algorithm is due to Ron Rivest. This code was
 written by Colin Plumb in 1993, no copyright is claimed.

Files: src/qcommon/crypto/sha-1/*
Copyright: 1998 Paul E. Jones <paulej@arid.us>
License: Freeware-Public-License
 All Rights Reserved.
 .
 This software is licensed as "freeware."  Permission to distribute
 this software in source and binary forms is hereby granted without
 a fee. THIS SOFTWARE IS PROVIDED 'AS IS' AND WITHOUT ANY EXPRESSED
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 THE AUTHOR SHALL NOT BE HELD LIABLE FOR ANY DAMAGES RESULTING
 FROM THE USE OF THIS SOFTWARE, EITHER DIRECTLY OR INDIRECTLY, INCLUDING,
 BUT NOT LIMITED TO, LOSS OF DATA OR DATA BEING RENDERED INACCURATE.
Comment:
 See also https://bugs.debian.org/730758 for a clarification of Paul E.
 Jones.

Files: src/qcommon/puff.h
Copyright:
  2002-2004 Mark Adler <madler@alumni.caltech.edu>
  2006 Joerg Dietrich <dietrich_joerg@gmx.de>
License: Zlib

Files: src/renderercommon/nanosvg/*
Copyright: 2013-2014 Mikko Mononen <memon@inside.org>
License: Zlib

Files: src/server/sv_wallhack.c
Copyright: 2012 Laszlo Menczel
License: GPL-2

Files: src/tools/shdr/tinydir.h
Copyright:
  2013-2016 Cong Xu, Lautis Sun, Baudouin Feildel, Andargor <andargor@yahoo.com>
License: BSD-2-clause

Files: debian/*
Copyright:
  2015 Markus Koschany <apo@debian.org>
  2022-2025 Sébastien Noel <sebastien@twolife.be>
License: GPL-3+

License: GPL-2
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License version 2 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License version
 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License version 3 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License version
 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License version
 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Kitware, Inc nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL KITWARE, INC BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

